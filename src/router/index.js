import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Styleguide from '@/components/Styleguide'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/styleguide',
      name: 'Styleguide',
      component: Styleguide
    }
  ]
})
