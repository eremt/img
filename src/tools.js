class Tools {
  constructor () {
    this.rect = {
      x: 0,
      y: 0,
      width: 0,
      height: 0
    }
  }
  mouse (e) {
    const {
      x,
      y,
      target
    } = e
    const rect = target.getBoundingClientRect()
    return {
      x: x - rect.x,
      y: y - rect.y
    }
  }
  selection () {
    const {
      x,
      y,
      width,
      height
    } = this.rect
    return {
      x,
      y,
      width,
      height
    }
  }
  crop () {
    let x, y, width, height
    return {
      key: 'crop',
      icon: '&#xE3C2;',
      down: (e) => {
        const mouse = this.mouse(e)
        x = mouse.x
        y = mouse.y
      },
      move: (e) => {
        console.log('Move tools, crop', this.mouse(e))
      },
      up: (e) => {
        const mouse = this.mouse(e)
        if (mouse.x > x) {
          width = mouse.x - x
        } else {
          width = x - mouse.x
          x = mouse.x
        }
        if (mouse.y > y) {
          height = mouse.y - y
        } else {
          height = y - mouse.y
          y = mouse.y
        }
        this.rect = Object.assign({}, { x, y, width, height })
        const c = e.target.getContext('2d')
        c.fillStyle = 'rgba(0, 0, 0, 0.5)'
        c.fillRect(x, y, width, height)
      }
    }
  }
  picker () {
    return {
      key: 'picker',
      icon: '&#xE3B8;',
      down: (e) => {
        console.log('Down tools, picker', this.mouse(e))
      },
      up: (e) => {
        console.log('Up tools, picker', this.mouse(e))
      }
    }
  }
}

export default new Tools()
